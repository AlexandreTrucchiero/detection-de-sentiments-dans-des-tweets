import pandas
from keras.utils import np_utils
import numpy as np
import random



# class parser , to load data for train
class Parser():
   
    def __init__(self):
        yolooo =  []
        uniqName = ["positive","negative","mixed","neutral"]
        names = []
        dataColor = []
        dataWhite = []
        with open("train_tweets_nn.txt",encoding="utf8") as fp:
            lines = fp.readlines()
            for line in lines:
                tabSplit = line.split("\t")
                if(not tabSplit[-1].isdigit()):
                    tabSplit.pop()
                names.append(tabSplit[1])
                dataColor.append([float(k) for k in tabSplit[2:]])
                
        
        
        self.__classChar2Int = { uniqName[i] : i for i in range(len(uniqName))}
        self.__classInt2Char = { i : uniqName[i] for i in range(len(uniqName))}
        self.__classToOutput = np_utils.to_categorical([self.__classChar2Int[i] for i in names])
        self.__dataInput = dataColor

    # Split data
    def getData(self,percent_train,percent_validation):
        numberOfDataTrain = int(len(self.__dataInput) * percent_train)
        numberOfDataValidation = int(len(self.__dataInput) * percent_validation)
        endOfDataValidation = numberOfDataValidation+numberOfDataTrain
        dataTrain = (np.asarray(self.__dataInput[0:numberOfDataTrain]),np.asarray(self.__classToOutput[0:numberOfDataTrain]))
        dataValidation = (np.asarray(self.__dataInput[numberOfDataTrain: endOfDataValidation]),np.asarray(self.__classToOutput[numberOfDataTrain: endOfDataValidation]))
        print(dataTrain[0][0])
        return dataTrain,dataValidation

    # Get number of input
    def getNbInput(self):
        return len(self.__dataInput[0])

# Class parser for model with 3 output
class Parser3Polarity():
    
    def __init__(self):
        uniqName = ["positive","negative","mixed","neutral"]
        names = []
        dataColor = []
        dataWhite = []
        with open("train_tweets_nn.txt") as fp:
            lines = fp.readlines()
            for line in lines:
                tabSplit = line.split("\t")
                #print(tabSplit)
                if(not tabSplit[-1].isdigit()):
                    tabSplit.pop()
                names.append(tabSplit[1])
                dataColor.append([float(k) for k in tabSplit[2:]])
                #print(dataColor)
                
                
        equivalent = {"positive":[1,0,0],"negative":[0,1,0],"mixed":[0.5,0.5,0],"neutral":[0,0,1]}
        
        self.__classChar2Int = { uniqName[i] : i for i in range(len(uniqName))}
        self.__classInt2Char = { i : uniqName[i] for i in range(len(uniqName))}
        self.__classToOutput = np_utils.to_categorical([self.__classChar2Int[i] for i in names])
        self.__classToOutput = np.array([equivalent[i] for i in names])
        self.__dataInput = dataColor

    # Split data
    def getData(self,percent_train,percent_validation):
        numberOfDataTrain = int(len(self.__dataInput) * percent_train)
        numberOfDataValidation = int(len(self.__dataInput) * percent_validation)
        endOfDataValidation = numberOfDataValidation+numberOfDataTrain
        dataTrain = (np.asarray(self.__dataInput[0:numberOfDataTrain]),np.asarray(self.__classToOutput[0:numberOfDataTrain]))
        dataValidation = (np.asarray(self.__dataInput[numberOfDataTrain: endOfDataValidation]),np.asarray(self.__classToOutput[numberOfDataTrain: endOfDataValidation]))
        print(dataTrain[0][0])
        return dataTrain,dataValidation

    # Get Number of input
    def getNbInput(self):
        return len(self.__dataInput[0])



