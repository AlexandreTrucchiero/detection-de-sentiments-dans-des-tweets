import sqlite3

import numpy as np
from gensim.models.doc2vec import Doc2Vec,TaggedDocument
from nltk.tokenize import word_tokenize
from time import time
from flask import Flask
from flask import request
import json
app = Flask(__name__,template_folder='templates')



from flask import render_template
from random import randint




tweetsDic = {}

# Load Tweet in memory
with open( "tweetsFiles/tweets_json.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]
    for tweet in tweets:
        idTweet = tweet["url"].split("/")[-1]
        tweetsDic[idTweet] = tweet
        

# Function for annotate
@app.route('/annote/<uapv>', methods = [ 'POST','GET'])
def find(uapv):
    if(uapv[-1] == "/"):
        uapv = uapv[:-1]
    conn = sqlite3.connect('annotation_rest/tweets.db')
    c = conn.cursor()
    # If user have summit a new annotation
    if request.method == 'POST' and request.form['submit_button'] != 'skip':
        idFromTweet = request.form["idTweet"]
        if(idFromTweet[-1] == "/"):
            idFromTweet = idFromTweet[:-1]   
        reqTweet = "select * from tweets where id="+idFromTweet
        #Insert a memory of tweet annotate
        c.execute('INSERT INTO user VALUES("'+uapv+'","'+idFromTweet+'")')
        # Insert the right polarity
        if request.form['submit_button'] == 'positive':
            c.execute(reqTweet)
            val = c.fetchall()[0][1]
            c.execute("UPDATE tweets set positive = "+str(val+1)+" where id="+idFromTweet)
            conn.commit()
        elif request.form['submit_button'] == 'negative':
            c.execute(reqTweet)
            val = c.fetchall()[0][2]
            c.execute("UPDATE tweets set negative = "+str(val+1)+" where id="+idFromTweet)
            conn.commit()
        elif request.form['submit_button'] == 'mixed':
            c.execute(reqTweet)
            val = c.fetchall()[0][3]
            c.execute("UPDATE tweets set mixed = "+str(val+1)+" where id="+idFromTweet)
            conn.commit()
        elif request.form['submit_button'] == 'neutral':
            c.execute(reqTweet)
            val = c.fetchall()[0][4]
            c.execute("UPDATE tweets set neutral = "+str(val+1)+" where id="+idFromTweet)
            conn.commit()
    # Select a new tweet to annoate
    c.execute('SELECT tweets.id FROM tweets')
    vals = c.fetchall()
    nbChoose = randint(0,len(vals))
    idChoose = vals[nbChoose][0]

    # Take the number of tweet annotate by this user
    c.execute('Select count(tweet) from user where id="'+uapv+'"')
    nbTweet = c.fetchall()[0][0]
    conn.close()
    tweetSelect = tweetsDic[idChoose]
    return render_template('index.html', idTweet=idChoose,idUser=uapv,textTweet=tweetSelect["message"],nbTweet=nbTweet,userTweet=tweetSelect["username"],likeTweet=tweetSelect["favoris"],rtTweet=tweetSelect["retweet"],dateTweet=tweetSelect["date"])










# root function
@app.route('/')
def root():
    return "url pour annoter : /annote/votreUapv"

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=3000,debug=True)