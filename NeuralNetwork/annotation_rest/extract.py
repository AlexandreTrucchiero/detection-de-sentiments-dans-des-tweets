import sqlite3
import json

# Load database
conn = sqlite3.connect('annotation_rest/tweets.db')
c = conn.cursor()



ref = {}
labelsPolarity = ["positive","negative","mixed","neutral"]

# Load Tweets
with open( "tweetsFiles/tweets_json.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]
    for tweet in tweets:
        idTweet = tweet["url"].split("/")[-1]
        ref[idTweet] = tweet

# Collect all tweets annotate
c.execute("Select distinct * from tweets where positive > 0.0 or negative > 0.0 or mixed > 0.0 or neutral > 0.0")
vals = c.fetchall()

countPolarity = {"positive":0,"negative":0,"neutral":0,"mixed":0}
result = []
for row in vals:
    score = [int(i) for i in row[1:]]
    nbZero = row.count(0)
    tweet = ref[row[0]]
    # If only one polarity
    if(nbZero == 3):
        tweet["polarity"] = labelsPolarity[score.index(max(score))]
        countPolarity[tweet["polarity"]]+=1
        result.append(tweet)
    


conn.close()

print("Nb annotate keep :",len(result))
print("Result polarity :",countPolarity)
file = open("tweetsFiles/tweetsAnnotateApi.json","w+",encoding="utf8")
tweetString = json.dumps({"tweets":result},ensure_ascii=False)
file.write(tweetString)
file.close()