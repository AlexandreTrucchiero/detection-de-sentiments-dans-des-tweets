import sqlite3
import json

# Load file sqlite3
conn = sqlite3.connect('annotation_rest/tweets.db')
c = conn.cursor()

# Create table
c.execute('''CREATE TABLE tweets
             (id text, positive real, negative real, mixed real, neutral real)''')
c.execute('''CREATE TABLE user
             (id text, tweet text)''')

ref = []
tweetsTestId = []
#Load tweets Test
with open( "tweetsFiles/tweetsTest.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]
    for tweet in tweets:
        idTweet = tweet["url"].split("/")[-1]
        tweetsTestId.append(idTweet)
# Load all tweets
with open( "tweetsFiles/tweets_json.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]
    for tweet in tweets:
        idTweet = tweet["url"].split("/")[-1]
        # if tweet not a tweets test
        if(idTweet not in tweetsTestId):
            # insert into database
            c.execute("INSERT INTO tweets VALUES ('"+idTweet+"',0,0,0,0)")

conn.commit()


conn.close()


