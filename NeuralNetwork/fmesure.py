from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation
from keras import losses
from keras import optimizers
from keras.datasets import mnist
from keras.utils import np_utils
from keras.callbacks import TensorBoard
from keras.callbacks import ModelCheckpoint
from Parser import Parser
import numpy as np
from gensim.models.doc2vec import Doc2Vec,TaggedDocument
from nltk.tokenize import word_tokenize
from time import time
from flask import Flask
from flask import request
import json
from trainFMesure import mean_f1

#Load Model
model = load_model("model/bestAccuracyFMesure.h5",custom_objects={"mean_f1":mean_f1})

# Load Data
parserData = Parser()
(x_train, y_train),(x_validation,y_validation) = parserData.getData(1,0)


# Predict
resultModel = model.predict(x_train)

# Calculate TP,FP,TN,FN
scoreTP = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

for ind in range(len(resultModel)):
    valuePredict = np.argmax(resultModel[ind])
    valueTrue = np.argmax(y_train[ind])
    if(valuePredict == valueTrue):
        scoreTP[valuePredict][0]+=1
        for i in range(4):
            if(i != valuePredict):
                scoreTP[i][2] += 1
    else:
        scoreTP[valuePredict][1]+=1
        scoreTP[valueTrue][3]+=1
        for i in range(4):
            if(i != valuePredict or i!= valueTrue):
                scoreTP[i][2] += 1

# calculate FMesure
fMesureList = []
for onescore in scoreTP:
    rappel = float(onescore[0]) / float(onescore[0]+onescore[3])
    presision = float(onescore[0]) / float(onescore[0]+onescore[1])
    fmesure= 2 * float(rappel*presision) / float(rappel+presision)
    fMesureList.append(fmesure)
print(scoreTP)
print(fMesureList)
print(sum(fMesureList)/4)

