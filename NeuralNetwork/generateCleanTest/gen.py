import json

# Load Tweet
file = open("tweetsFiles/tweets_json.json","r",encoding="utf8")
tweets = file.read()
allTweets = json.loads(tweets)["tweets"]
file.close()

ref = {}
for i in allTweets:
    idTweet = str(i["url"].split("/")[-1])
    i["id"] = idTweet
    ref[idTweet] = i

# Load file with id of tweet test
cleanFile = []
file = open("generateCleanTest/test.txt","r",encoding="utf8")
lines = file.read()
file.close()
for line in lines.split("\n"):
    if(line==''):continue
    idT = str(line.split(" ")[0])
    tweetFind = ref[idT]
    if(tweetFind == None):
        print("Erreur tweet id "+idT)
    cleanFile.append(tweetFind)

jsonFinal = {"tweets" : cleanFile}

# Save
file = open("tweetsFiles/tweetsTest.json","w+",encoding="utf8")
tweetString = json.dumps(jsonFinal,ensure_ascii=False)
file.write(tweetString)
file.close()

