from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation
from keras import losses
from keras import optimizers
from keras.datasets import mnist
from keras.utils import np_utils
from keras.callbacks import TensorBoard
from keras.callbacks import ModelCheckpoint
from Parser import Parser
import numpy as np
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
from time import time
from flask import Flask
from flask import request
import json
from tokenizer import tokenize, tokenizePlus
app = Flask(__name__)
uniqName = ["positif", "negatif", "mixte", "autre"]

# Load model doc2vec
modelDoc2vec = Doc2Vec.load("model/d2vLem.model") 


# Fonction to predict Polarity With the neural network
@app.route('/find', methods = [ 'POST'])
def find():

    print("Process Predict...",end="",flush=True)
    result = { }
    #Load model NN
    model = load_model("model/bestAccuracy.h5")
    tabString = request.get_json()["tweets"]
    tabValueToPredict = np.array([np.array(list(k[1:])) for k in tabString])
    tabId = [k[0] for k in tabString]

    resultModel = model.predict(tabValueToPredict)

    allPolaritySum = {k: 0 for k in uniqName}

    # Transform result
    for onePredictIndex in range(len(resultModel)):
        line = list(resultModel[onePredictIndex])
        
        result[tabId[onePredictIndex]] = uniqName[line.index(max(line))]
        allPolaritySum[result[tabId[onePredictIndex]]] += 1
    print("Done")
    print("Result :", allPolaritySum)
    return json.dumps(result)



# Fonction to predict with a model using 3 output node
@app.route('/find3', methods = [ 'POST'])
def find3():

    uniqNameLess = ["positif","negatif","autre"]
    print("Process Predict...",end="",flush=True)
    result = { }
    # Load the model
    model = load_model("model/bestAccuracy3Polarity.h5")
    tabString =  request.get_json()["tweets"]
    tabValueToPredict = np.array([ np.array(list(k[1:])) for k in tabString])
    tabId = [ k[0] for k in tabString]

    # predict
    resultModel = model.predict(tabValueToPredict)

    allPolaritySum = { k : 0 for k in uniqName}

    # Transform value
    for onePredictIndex in range(len(resultModel)):
        line = list(resultModel[onePredictIndex])
        polariryCheck = uniqNameLess[line.index(max(line))]

        # IF negative and postive was hight score then tweet = mixed
        if(line[0]>0.18 and line[1]>0.18):
            polariryCheck = "mixte"
        result[tabId[onePredictIndex]] = polariryCheck
        allPolaritySum[polariryCheck] += 1
    print("Done")
    print("Result :",allPolaritySum)
    return json.dumps(result)


# Function to transform message to vector thanks to Doc2Vec
@app.route('/message2vec', methods = [ 'POST'])
def message2vec():
    print("Process message 2 vec...", end="", flush=True)
    result = {}
    tabString = request.get_json()["tweets"]
    for vals in tabString:
        valTokenize = tokenizePlus(vals[1]) #<-------- Tokenizer lourd
        vector = [float(i) for i in list(modelDoc2vec.infer_vector(valTokenize))] # <--------- result array float
        result[vals[0]] = vector 
    print("Done")
    return json.dumps(result)


@app.route('/')
def root():
    return "Server on"


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3000, debug=True)
