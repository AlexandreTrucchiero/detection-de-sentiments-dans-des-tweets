import json
import unidecode
import re
import numpy as np
from math import floor
from random import shuffle


tweets =[]
#Load Train tweets
with open( "tweetsFiles/tweetsTrain.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]

twPos =[]
twNeg = []
twNeu = []
twMix = []
# Filter tweet by polarity
for i in tweets:
    if(i["polarity"] == "negative"):
        twNeg.append(i)
    elif(i["polarity"] == "positive"):
        twPos.append(i)
    elif(i["polarity"] == "mixed"):
        twMix.append(i)
    elif(i["polarity"] == "neutral"):
        twNeu.append(i)

# Calculated coef for balance
negLen = len(twNeg)
coefPos = floor(negLen / len(twPos))
coefNeu = floor(negLen / len(twNeu))
coefMix = floor(negLen / len(twMix))

# Equilibrate
listFinal = twNeg + coefPos*twPos + coefNeu*twNeu + coefMix*twMix
print("Start with "+str(len(tweets))+ " tweets")
print("End with "+str(len(listFinal))+" tweets")
print("Negative :", negLen)
print("Positive :", len(twPos),"->",len(twPos)*coefPos)
print("Mixed :", len(twMix),"->",len(twMix)*coefMix)
print("Neutral :", len(twNeu),"->",len(twNeu)*coefNeu)
shuffle(listFinal)
final = {"tweets":listFinal}

# Save
file = open("tweetsFiles/tweetsTrainBalance.json","w+",encoding="utf8")
tweetString = json.dumps(final,ensure_ascii=False)
file.write(tweetString)
file.close()
