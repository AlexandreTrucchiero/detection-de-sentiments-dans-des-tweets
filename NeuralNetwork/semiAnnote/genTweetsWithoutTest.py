import json
import unidecode
import re
import numpy as np

tweets =[]
with open( "tweetsFiles/tweets_json.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]

tweetsToPut = []


tweetsTest = []

yolo = {"negative":0,"neutral":0,"positive":0,"mixed":0}

with open( "tweetsFiles/tweetsTest.json",encoding="utf8") as file:
    tmpTweetsTest = json.load(file)["tweets"]
    for oneTweet in tmpTweetsTest:
        tweetsTest.append(oneTweet["url"])

for tweet in tweets:
    if(tweet["url"] in tweetsTest):
        continue
    tweetsToPut.append(tweet)
    


fileEnd = {"tweets":tweetsToPut}

file = open("tweetsFiles/tweetsTrainNoPolarity.json","w+",encoding="utf8")
tweetString = json.dumps(fileEnd,ensure_ascii=False)
file.write(tweetString)
file.close()
