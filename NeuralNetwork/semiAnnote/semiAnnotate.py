import json
import unidecode
import re
import numpy as np
import pandas as pd

emoji = pd.read_csv("semiAnnote/emoji.csv",sep="\t")

listPolarity = ["positive","negative","mixed","neutral"]


mixedBad =["null","incompetent"]
mixedBon = ["meilleur","genial","exceptionnel"]


# regex all polarity
negativeRegex = [r"vide",r"mauvais",r"classe politique",r'enfant',r'connerie',r'tele realite',r'nazi',r'nullite',r'vomir',r'fachosphere']
positiveRegex = [r'excellent']
mixedRegex = [r"ecrase",r"mieux que"]
neutralRegex  =[r"#sondage"]


for i in mixedBad:
    for j in mixedBon:
        mixedRegex.append(i+".*[^pas] "+j)
        mixedRegex.append("[^pas] "+j+".*"+i)


negativePattern = []
positivePattern = []
mixedPattern = []
neutralPattern  =[]

for regex in negativeRegex:
    negativePattern.append(re.compile(regex))
for regex in positiveRegex:
    positivePattern.append(re.compile(regex))
for regex in mixedRegex:
    mixedPattern.append(re.compile(regex,re.U))
for regex in neutralRegex:
    neutralPattern.append(re.compile(regex,re.U))
tweets =[]

#load tweets
with open( "tweetsFiles/tweets_json.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]

tweetsAnnotate = []
tweetsNoAnnotate = []

tweetsTest = []

countPolarityDic = {"negative":0,"neutral":0,"positive":0,"mixed":0}

# load tweets Test
with open( "tweetsFiles/tweetsTest.json",encoding="utf8") as file:
    tmpTweetsTest = json.load(file)["tweets"]
    for oneTweet in tmpTweetsTest:
        tweetsTest.append(oneTweet["url"])

for tweet in tweets:
    if(tweet["url"] in tweetsTest):
        continue
   
    text = unidecode.unidecode(tweet["message"].lower())
    polarity = None
    # Check emoji
    emojiFind = re.findall(r"(?:[\U00010000-\U0010ffff])",tweet["message"])
    if(len(emojiFind) != 0):
        score = [0,0,0,0]
        for indexEmoji  in range(len(emojiFind)):
            oneEmoji = emojiFind[indexEmoji]
            if(oneEmoji =="👌" and indexEmoji != 0 and emojiFind[indexEmoji-1] == "👉"):
                score[0] += -2
                score[1] += 2
            
            result = emoji.loc[emoji['emoji'] == oneEmoji]
            if(result.empty):
                continue
            score[0] += int(result["positive"].values)
            score[1] += int(result["negative"].values)
            score[2] += int(result["mixed"].values)
            score[3] += int(result["neutral"].values)

        countZero = score.count(0)
        if(countZero == 3 ):
            polarity = listPolarity[score.index(max(score))]
        """elif(countZero == 2):
            scoreTmp = list(score)
            scoreTmp.remove(0)
            scoreTmp.remove(0)
            if(abs(scoreTmp[0]- scoreTmp[1] ) > 2):
                polarity = listPolarity[scoreTmp.index(max(scoreTmp))]"""
        """else:
                if(score[2] > 0):
                    print(tweet)
                    polarity = "mixed"""
        """if(polarity == "" and countZero != 4 and score[1] == score[0] and score[1] > 0):
            print(tweet)"""
        maxScoreEmoji = max(score)

        scoreTransform = [score[i] - sum(score) for i in range(len(score))]

    # Check regex
    if(polarity == None):
        #Negative
        for pattern in negativePattern:
            if pattern.search(text):
                polarity = "negative"
                break

        #Neutral
        for pattern in neutralPattern:
            if pattern.search(text):
                polarity = "neutral"
                break

        #Positive
        for pattern in positivePattern:
            if pattern.search(text):
                polarity = "positive"
                break

         #Mixed
        if(re.search(r"(?:(?:mlp|lepen|marine).*(?:macron|emmanuel))|(?:(?:macron|emmanuel).*(?:mlp|lepen|marine))",text)):
            for pattern in mixedPattern:
                if pattern.search(text):
                    polarity = "mixed"
                    break

    # Somme username can provide only neutral tweet
    if(tweet["username"] == "BFMTV" or "lemonde" in tweet["username"].lower()):
        polarity = "neutral"

    if(polarity):
        tweet["polarity"] = polarity
        countPolarityDic[polarity]+=1
        tweetsAnnotate.append(tweet)
    else:
        tweetsNoAnnotate.append(tweet)



print("Tweet semi annotate : "+str(sum([countPolarityDic[key] for key in countPolarityDic])))
print("Repartition :" +str(countPolarityDic))


# Load tweet annotated manually
tweetsManuel =[]
with open( "tweetsFiles/tweetsPolarity.json",encoding="utf8") as file:
    tweetsManuel = json.load(file)["tweets"]
    for tweet in tweetsManuel:
        countPolarityDic[tweet["polarity"]] += 1
fileEnd = {"tweets":tweetsAnnotate+tweetsManuel}

print("Tweet semi annotate + manuel : "+str(sum([countPolarityDic[key] for key in countPolarityDic])))
print("Repartition :" +str(countPolarityDic))

# Save
file = open("tweetsFiles/tweetsTrain.json","w+",encoding="utf8")
tweetString = json.dumps(fileEnd,ensure_ascii=False)
file.write(tweetString)
file.close()
