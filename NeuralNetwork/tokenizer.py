
import nltk
from nltk.corpus import stopwords
from nltk.sentiment import vader
import json
import operator
import re
import spacy
import enchant
from googletrans import Translator
from nltk.corpus import wordnet,stopwords
from nltk.stem import WordNetLemmatizer
import os
import subprocess
stopWordFrench = stopwords.words("french")
stopWordFrench.remove("pas")
stopWordFrench.append("avoir")
lemmEnglish = WordNetLemmatizer()
print("Load Lemmatizer....")
nlp  =spacy.load("fr_core_news_md")
print("\tDone")

dictEnchant = enchant.Dict("fr_FR")

translator = Translator()

dictionnaireTranslate = {}
with open("wordTranslate/wordTranslate.json",encoding="utf8") as f:
    dictionnaireTranslate = json.load(f)

re_at = r"(?:@[\w_]+)"
re_hashtag = r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)"
re_emot = r"(?:[<>]?[:;=8][\-o\*\']?[\)\]\(\[dDpP/\:\}\{@\|\\]|[\)\][(\[dDpP/\:\}\{@\|\\][\-o\*\']?[:;=8][<>]?)"
re_url = r"(?:http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)"
re_cash = r"(?:\$+[\w_]+[\w\'_\-]*[\w_]+)"
re_chiffre = r"(?:[+\-]?\d+[,/.:-]\d+[+\-]?|\d+)"
re_mot = r"(?:[\p{L}-]+)"
re_mot = r"(?:[\w_]+)"
re_point = r"(?:\.(?:\s*\.){1,})"
re_inter = r"(?:\?(?:\s*\?){1,})"
re_excla = r"(?:\!(?:\s*\!){1,})"
re_reste = r"(?:\S+)"
re_emoji = r"(?:[\U00010000-\U0010ffff])"
regexFinal = re.compile(r"%s" % "("+re_url+"|"+re_at+"|"+re_hashtag+"|"+re_cash+"|"+re_emot+"|"+re_emoji+"|"+re_chiffre+"|"+re_mot+"|"+re_point+"|"+re_inter+"|"+re_excla+"|"+re_reste+")",re.UNICODE|re.IGNORECASE)
#print(regexFinal)
#count = 0


def tokenizeSimple(message):
    """global count
    count+=1
    print(count,end="\r",flush=True)"""
    lowered = message.lower()
    #tokenSpace  = message.split(" ")
    tokens = re.findall(regexFinal,lowered)
    result =[]

    for token in tokens:
        transformToken = token
        if(token.startswith("http")):
            transformToken = "url_http"
        elif(re.search(r"^[a-zA-Z]",token)):
            transformToken = token
        elif(token.startswith(".") and len(token)>1):
            transformToken = "..."
        elif(token.startswith("?") and len(token)>1):
            transformToken = "???"
        elif(token.startswith("!") and len(token)>1):
            transformToken = "!!!"
        """elif(token.startswith("#")):
            if ("debat" in token):
                transformToken = "#debat"
            # si le temps faire transformation #macron #macronEmmanuel en normal"""
        result.append(transformToken)
   
    return result

def tokenize(message):
    """global count
    count+=1
    print(count,end="\r",flush=True)"""
    lowered = message.lower()
    #tokenSpace  = message.split(" ")
    tokens = re.findall(regexFinal,lowered)
    result =[]

    for token in tokens:
        transformToken = token
        if(token.startswith("http")):
            transformToken = "url_http"
        elif(re.search(r"^[a-zA-Z]",token)):
            transformToken = nlp(token)[0].lemma_
        elif(token.startswith(".") and len(token)>1):
            transformToken = "..."
        elif(token.startswith("?") and len(token)>1):
            transformToken = "???"
        elif(token.startswith("!") and len(token)>1):
            transformToken = "!!!"
        """elif(token.startswith("#")):
            if ("debat" in token):
                transformToken = "#debat"
            # si le temps faire transformation #macron #macronEmmanuel en normal"""
        result.append(transformToken)
   
    return result



def tokenizePlus(message):
    """global count
    count+=1
    print(count,end="\r",flush=True)"""
    lowered = message.lower()
    #tokenSpace  = message.split(" ")
    tokens = re.findall(regexFinal,lowered)
    result =[]

    for token in tokens:
        if(token in stopWordFrench):
            continue
        transformToken = token
        if(token.startswith("http")):
            transformToken = "url_http"
        elif(re.search(r"^[a-zA-Z]",token)):
            transformToken = nlp(token)[0].lemma_
            if(transformToken in stopWordFrench):
                continue
        elif(token.startswith(".") and len(token)>1):
            transformToken = "..."
        elif(token.startswith("?") and len(token)>1):
            transformToken = "???"
        elif(token.startswith("!") and len(token)>1):
            transformToken = "!!!"
        """elif(token.startswith("#")):
            if ("debat" in token):
                transformToken = "#debat"
            # si le temps faire transformation #macron #macronEmmanuel en normal"""
        result.append(transformToken)
   
    return result

def tokenizePlusCorrect(message):
    """global count
    count+=1
    print(count,end="\r",flush=True)"""
    lowered = message.lower()
    #tokenSpace  = message.split(" ")
    tokens = re.findall(regexFinal,lowered)
    result =[]

    for token in tokens:
        if(token in stopWordFrench):
            continue
        transformToken = token
        if(token.startswith("http")):
            transformToken = "url_http"
        elif(re.search(r"^[a-zA-Z]",token)):
            if(not dictEnchant.check(token)):
                    listEnchant = dictEnchant.suggest(token)
                    if( len(listEnchant) != 0):
                        token = listEnchant[0]
                        if(token in stopWordFrench):
                            continue
                    del listEnchant
            transformToken = nlp(token)[0].lemma_
            
            if(transformToken in stopWordFrench):
                continue
        elif(token.startswith(".") and len(token)>1):
            transformToken = "..."
        elif(token.startswith("?") and len(token)>1):
            transformToken = "???"
        elif(token.startswith("!") and len(token)>1):
            transformToken = "!!!"
        """elif(token.startswith("#")):
            if ("debat" in token):
                transformToken = "#debat"
            # si le temps faire transformation #macron #macronEmmanuel en normal"""
        result.append(transformToken)
   
    return result




count = 0
def tokenizeHypernyms(message):
    #global dictionnaireTranslate
    global count
    count+=1
    print(count,end="\r",flush=True)
    lowered = message.lower()
    #tokenSpace  = message.split(" ")
    tokens = re.findall(regexFinal,lowered)
    result =[]

    for token in tokens:
        if(token in stopWordFrench):
            continue
        transformToken = token
        #print(token,end = " ")
        if(token.startswith("http")):
            transformToken = "url_http"
        elif(re.search(r"^[a-zA-Z]",token)):
            
            if("macron" in transformToken):
                transformToken = "macron"
            elif("lepen" in transformToken):
                transformToken = "lepen"
            else:
                word = transformToken
                if(not dictEnchant.check(word)):
                    listEnchant = dictEnchant.suggest(word)
                    if( len(listEnchant) != 0):
                        word = listEnchant[0]
                        if(word in stopWordFrench):
                            continue
                    del listEnchant
                lem = nlp(word)[0].lemma_
                if(lem in stopWordFrench):
                    continue
                if(not dictEnchant.check(lem)):
                    listEnchant = dictEnchant.suggest(lem)
                    if( len(listEnchant) != 0):
                        lem = listEnchant[0]
                        if(lem in stopWordFrench):
                            continue
                    del listEnchant
                #print("-",lem)
                trad = translateWord(lem)
                """if(lem in dictionnaireTranslate):
                    trad = dictionnaireTranslate[lem]
                else:
                    trad = translateWord(lem)
                    #if(trad )
                    dictionnaireTranslate[lem] = trad"""
                #print(trad,end = " ")
                wordEnglish = trad.split(" ")[-1]
                synsetList = wordnet.synsets(wordEnglish)
                if(len(synsetList)!=0):
                    synset = synsetList[0]
                    hypernymsList = synset.hypernyms()
                    if(len(hypernymsList)!=0):
                        hypernym = hypernymsList[0]
                        transformToken = hypernym.name().split(".")[0]
                        del hypernym
                    else:
                        transformToken = synset.name().split(".")[0]
                    
                    del hypernymsList
                    del synset
                else:
                    transformToken = lem
                del synsetList
                del trad
                del word
                del lem
            #transformToken = nlp(token)[0].lemma_
        elif(token.startswith(".") and len(token)>1):
            transformToken = "..."
        elif(token.startswith("?") and len(token)>1):
            transformToken = "???"
        elif(token.startswith("!") and len(token)>1):
            transformToken = "!!!"
        """elif(token.startswith("#")):
            if ("debat" in token):
                transformToken = "#debat"
            # si le temps faire transformation #macron #macronEmmanuel en normal"""
        result.append(transformToken)
        del transformToken
        #print(transformToken)
    return result


def saveDict():
    f = open("wordTranslate/wordTranslate.json","w+")
    f.write(json.dumps(dictionnaireTranslate))
    f.close()



def translateWord(word):
    global dictionnaireTranslate
    word = word.lower()
    if(word in dictionnaireTranslate):
        return dictionnaireTranslate[word]

    translationDict = os.popen('dict -d fd-fra-eng ' + word).read()
    splitText = translationDict.split("\n\n")
    trans  =""
    if(len(splitText)<2):
        translation = os.popen("echo \""+word+"\"| apertium fr-es").read().strip()
        if(translation.startswith("*")):
            print("-------------------",flush=True)
            trans = input("translate "+word+" : ").lower()
            print("-------------------",flush=True)
            dictionnaireTranslate[word] = trans
            saveDict()
        else:
            translationFr = os.popen("echo \""+translation+"\"| apertium es-en").read().strip()
            if(translationFr.startswith("*")):
                print("-------------------",flush=True)
                trans = input("translate "+word+" : ").lower()
                print("-------------------",flush=True)
                dictionnaireTranslate[word] = trans
                saveDict()
            else:
                trans = translationFr.lower().split(" ")[-1]
                dictionnaireTranslate[word] = trans
    else:
        trans = splitText[2].split("\n")[1].split(",")[0].split(" ")[-1].strip()
        dictionnaireTranslate[word] = trans

    
            
        

    return trans

def translateWordMem(word):
    global dictionnaireTranslate

    if(word in dictionnaireTranslate):
        return dictionnaireTranslate[word]

    translation = os.popen('dict -d fd-fra-eng ' + word).read()
    splitText = translation.split("\n\n")
    trans  =""
    if(len(splitText)<2):
        print("-------------------",flush=True)
        trans = input("translate "+word+" : ")
        print("-------------------",flush=True)
        dictionnaireTranslate[word] = trans
        saveDict()
    else:
        trans = splitText[2].split("\n")[1].split(",")[0].split(" ")[-1].strip()
        dictionnaireTranslate[word] = trans

    return trans


