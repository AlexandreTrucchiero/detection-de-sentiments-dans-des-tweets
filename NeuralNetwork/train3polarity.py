#!/usr/bin/env python
# -*- coding: utf-8 -*-
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation, Dropout
from keras import losses
from keras import optimizers
from Parser import Parser, Parser3Polarity
from keras.utils import np_utils
from keras.callbacks import TensorBoard
from keras.callbacks import ModelCheckpoint
from random import randint
from time import time
import os
import keras.backend as K
import tensorflow as tf
from sklearn.metrics import f1_score


# Load data
parserData = Parser3Polarity()
(x_train, y_train),(x_validation,y_validation) = parserData.getData(0.8,0.2)


# definition model
# 3 output: posituve, negative,neutral
model = Sequential()

model.add(Dense(200, input_dim=parserData.getNbInput(), activation='relu'))
model.add(Dense(300, activation='tanh'))
model.add(Dense(150, activation='relu'))
model.add(Dense(3, activation='softmax'))
model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=["categorical_accuracy"])


checkpointAccuracy = ModelCheckpoint("model/bestAccuracy3Polarity.h5",monitor="val_categorical_accuracy",verbose=0,save_best_only=True,save_weights_only=False)



# Train
history = model.fit(x_train, y_train, nb_epoch=150, verbose=1, validation_data=(x_validation, y_validation), callbacks=[checkpointAccuracy])
print(history)
