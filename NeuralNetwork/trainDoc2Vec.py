from gensim.models.doc2vec import Doc2Vec,TaggedDocument
from nltk.tokenize import word_tokenize
import json
from tokenizer import tokenize, tokenizeHypernyms, saveDict,tokenizePlus,tokenizePlusCorrect

# Inspired of https://medium.com/@mishra.thedeepak/doc2vec-simple-implementation-example-df2afbbfbad5

data = []
# Load tweets training
with open( "tweetsFiles/tweetsTrainNoPolarity.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]

    for tweet in tweets:
        data.append(tweet["message"])

# Tag each document
tagged_data = [TaggedDocument(words=tokenizePlusCorrect(_d.lower()), tags=[str(i)]) for i, _d in enumerate(data)]

# Save dictionnary of translation ( use only wth hypernyms )
saveDict()

max_epochs = 100
vec_size = 20
alpha = 0.025

# Define the model.
model = Doc2Vec(size=vec_size,
                alpha=alpha, 
                min_alpha=0.00025,
                min_count=1,
                dm =1)

# Define voculary for the model
model.build_vocab(tagged_data)

# Train the model
for epoch in range(max_epochs):
    print('iteration {0}'.format(epoch))
    model.train(tagged_data,
                total_examples=model.corpus_count,
                epochs=model.iter)
    # decrease the learning rate
    model.alpha -= 0.0002
    # fix the learning rate, no decay
    model.min_alpha = model.alpha

# Save
model.save("model/d2vPlusCorrect.model")
print("Model Saved")