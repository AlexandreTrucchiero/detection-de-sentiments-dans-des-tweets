from gensim.models.doc2vec import Doc2Vec,TaggedDocument
from nltk.tokenize import word_tokenize
import json
from tokenizer import tokenize, tokenizeHypernyms, saveDict,tokenizePlus,tokenizePlusCorrect

# Inspired of https://medium.com/@mishra.thedeepak/doc2vec-simple-implementation-example-df2afbbfbad5


# Load tweets train
data = []
with open( "tweetsFiles/tweetsTrain.json",encoding="utf8") as file:
    tweets = json.load(file)["tweets"]

    for tweet in tweets:
        data.append((tweet["polarity"],tweet["message"]))

# Tag all tweets with a polarity
tagged_data = [TaggedDocument(words=tokenizePlusCorrect(_d.lower()), tags=[str(i)]) for i, _d in data]

saveDict()

max_epochs = 100
vec_size = 20
alpha = 0.025

# create model
model = Doc2Vec(size=vec_size,
                alpha=alpha, 
                min_alpha=0.00025,
                min_count=1,
                dm =1)
  
model.build_vocab(tagged_data)

# train
for epoch in range(max_epochs):
    print('iteration {0}'.format(epoch))
    model.train(tagged_data,
                total_examples=model.corpus_count,
                epochs=model.iter)
    # decrease the learning rate
    model.alpha -= 0.0002
    # fix the learning rate, no decay
    model.min_alpha = model.alpha

model.save("model/d2vPlusCorrectPolarity.model")
print("Model Saved")