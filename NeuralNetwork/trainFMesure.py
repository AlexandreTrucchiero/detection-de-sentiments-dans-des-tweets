#!/usr/bin/env python
# -*- coding: utf-8 -*-
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation, Dropout
from keras import losses
from keras import optimizers
from Parser import Parser, Parser3Polarity
from keras.utils import np_utils
from keras.callbacks import TensorBoard
from keras.callbacks import ModelCheckpoint
from random import randint
from time import time
import os
import keras.backend as K
import tensorflow as tf
from sklearn.metrics import f1_score


# Calculate F Mesure with tensor
def recall_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

def precision_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


def mean_f1(y_true, y_pred):
    
    
    tensorPositiveTrue = y_true[:,:1]
    tensorPositivePred = y_pred[:,:1]
    
    tensorNegativeTrue = y_true[:,1:2]
    tensorNegativePred = y_pred[:,1:2]
    tensorMixedTrue = y_true[:,2:3]
    tensorMixedPred = y_pred[:,2:3]
    tensorNeutralTrue = y_true[:,3:4]
    tensorNeutralPred = y_pred[:,3:4]
    f1Positive = f1_m(tensorPositiveTrue, tensorPositivePred)
    f1Negative = f1_m(tensorNegativeTrue, tensorNegativePred)
    f1Mixed = f1_m(tensorMixedTrue, tensorMixedPred)
    f1Neutral = f1_m(tensorNeutralTrue, tensorNeutralPred)
    
    return (f1Positive+f1Negative+f1Neutral+f1Mixed )/ 4# K.mean(allTensor)
    #return f1Positive


def mean_f1_loss(y_true, y_pred):
    
    return 1-mean_f1(y_true, y_pred)


if __name__ == "__main__":

    parserData = Parser()
    (x_train, y_train),(x_validation,y_validation) = parserData.getData(0.8,0.2)


    # definition model
    # 4 sortie
    model = Sequential()

    model.add(Dense(200, input_dim=parserData.getNbInput(), activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(300, activation='tanh'))
    #model.add(Dropout(0.8))
    model.add(Dense(150, activation='relu'))

    #model.add(Dropout(0.5))
    model.add(Dense(4, activation='softmax'))
    
    model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=["categorical_accuracy",mean_f1])


    checkpointAccuracy = ModelCheckpoint("model/bestAccuracyFMesure.h5",monitor="val_categorical_accuracy",verbose=0,save_best_only=True,save_weights_only=False)



    # Train
    history = model.fit(x_train, y_train, nb_epoch=150, verbose=1, validation_data=(x_validation, y_validation), callbacks=[checkpointAccuracy])
    print(history)
