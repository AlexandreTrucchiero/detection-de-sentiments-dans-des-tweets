#!/usr/bin/env python
# -*- coding: utf-8 -*-
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation, Dropout, BatchNormalization
from keras import losses
from keras import optimizers
from Parser import Parser, Parser3Polarity
from keras.utils import np_utils
from keras.callbacks import TensorBoard
from keras.callbacks import ModelCheckpoint
from random import randint
from time import time
import os
import keras.backend as K
import tensorflow as tf
from sklearn.metrics import f1_score
from keras.regularizers import l2, l1

#Load data
parserData = Parser()
(x_train, y_train),(x_validation,y_validation) = parserData.getData(0.8,0.2)


# Definition model
# 4 output => negative, positive, mixed, neutral
model = Sequential()
#model.add(BatchNormalization(input_shape=(parserData.getNbInput(),)))
model.add(Dense(200, activation='relu',input_dim=parserData.getNbInput()))
#model.add(BatchNormalization())
#model.add(Dropout(0.5))
model.add(Dense(300, activation='tanh'))
#model.add(BatchNormalization())
#model.add(Dropout(0.5))
model.add(Dense(150, activation='relu'))
#model.add(BatchNormalization())
#model.add(Dropout(0.5))
model.add(Dense(4, activation='softmax'))
'categorical_crossentropy'
model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=["categorical_accuracy"])



checkpointAccuracy = ModelCheckpoint("model/bestAccuracy.h5",monitor="val_categorical_accuracy",verbose=0,save_best_only=True,save_weights_only=False)



# Train
history = model.fit(x_train, y_train, nb_epoch=200, verbose=1, validation_data=(x_validation, y_validation), callbacks=[checkpointAccuracy])
