package org.app;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.entity.Tweet;
import org.performance.PerformanceManager;
import org.tools.Configuration;
import org.tools.FileManager;
import org.tools.ScoreManager;
import org.tools.TweetManager;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author alexandre trucchiero
 *
 */
public class App {

	public static void main(String[] args) throws SAXException, Exception {
		PerformanceManager.start();

		// findPolarity();
	}


	/**
	 * Find polarity of tweet(s) using API request
	 * 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void findPolarity() throws JsonParseException, JsonMappingException, IOException {
		List<Tweet> tweets = FileManager.loadTweetJsonFile(Configuration.TEST_FILE_PATH);
		ScoreManager.computeAllScores(tweets);
		Map<String, String> results = TweetManager.postFindPolarity(tweets);
		FileManager.writeResultFile("query_results_nn.txt", results);

	}
}
