package org.entity;

/**
 * @author alexandre trucchiero
 *
 */
public class PolarityObject {
	protected String name;
	protected Double positiveScore;
	protected Double negativeScore;
	protected Double mixedScore;
	protected Double neutralScore;

	public PolarityObject(String name, Double positiveScore, Double negativeScore, Double mixedScore,
			Double neutralScore) {
		this.name = name;
		this.positiveScore = positiveScore;
		this.negativeScore = negativeScore;
		this.mixedScore = mixedScore;
		this.neutralScore = neutralScore;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPositiveScore() {
		return positiveScore;
	}

	public void setPositiveScore(Double positiveScore) {
		this.positiveScore = positiveScore;
	}

	public Double getNegativeScore() {
		return negativeScore;
	}

	public void setNegativeScore(Double negativeScore) {
		this.negativeScore = negativeScore;
	}

	public Double getMixedScore() {
		return mixedScore;
	}

	public void setMixedScore(Double mixedScore) {
		this.mixedScore = mixedScore;
	}

	public Double getNeutralScore() {
		return neutralScore;
	}

	public void setNeutralScore(Double neutralScore) {
		this.neutralScore = neutralScore;
	}
}
