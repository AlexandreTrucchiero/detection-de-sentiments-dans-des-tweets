package org.entity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author alexandre trucchiero
 *
 */
public class Tweet {

	private String id;
	private String user;
	private String message;
	private String date;
	private String polarity;
	private List<String> tokens;
	private Double[] hashScore;
	private Double[] emojiScore;
	private Double[] wordScore;
	private Double[] svmScore;
	private List<Double> wordEmbeddingScoreList;
	private Double uppercaseScore;
	private Double letterLengthScore;
	private Double wordLengthScore;
	private Double timestampScore;

	public Tweet(String id, String user, String message, String date) {
		this.id = id;
		this.user = user;
		this.message = message;
		this.date = date;
		this.polarity = "undefined";
		this.hashScore = new Double[4];
		Arrays.fill(this.hashScore, 0.0);
		this.emojiScore = new Double[4];
		Arrays.fill(this.emojiScore, 0.0);
		this.wordScore = new Double[4];
		Arrays.fill(this.wordScore, 0.0);
		this.svmScore = new Double[4];
		Arrays.fill(this.svmScore, 0.0);
		this.wordEmbeddingScoreList = new LinkedList<>();
		this.uppercaseScore = 0d;
		this.letterLengthScore = 0d;
		this.wordLengthScore = 0d;
		this.timestampScore = 0d;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPolarity() {
		return polarity;
	}

	public void setPolarity(String polarity) {
		this.polarity = polarity;
	}

	public List<String> getTokens() {
		return tokens;
	}

	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}

	public Double[] getHashScore() {
		return hashScore;
	}

	public Double getHashScore(int index) {
		return hashScore[index];
	}

	public void setHashScore(Double hashScore, int index) {
		this.hashScore[index] = hashScore;
	}

	public void incrementHashScore(Double hashScore, int index) {
		this.hashScore[index] += hashScore;
	}

	public Double[] getEmojiScore() {
		return emojiScore;
	}

	public Double getEmojiScore(int index) {
		return emojiScore[index];
	}

	public void setEmojiScore(Double emojiScore, int index) {
		this.emojiScore[index] = emojiScore;
	}

	public void incrementEmojiScore(Double emojiScore, int index) {
		this.emojiScore[index] += emojiScore;
	}

	public Double[] getWordScore() {
		return wordScore;
	}

	public Double getWordScore(int index) {
		return wordScore[index];
	}

	public void setWordScore(Double wordScore, int index) {
		this.wordScore[index] = wordScore;
	}

	public void incrementWordScore(Double wordScore, int index) {
		this.wordScore[index] += wordScore;
	}

	public Double[] getSvmScore() {
		return svmScore;
	}

	public Double getSvmScore(int index) {
		return svmScore[index];
	}

	public void setSvmScore(int index, Double value) {
		this.svmScore[index] = value;
	}

	public List<Double> getWordEmbeddingScoreList() {
		return wordEmbeddingScoreList;
	}

	public void setWordEmbeddingScoreList(List<Object> list) {
		wordEmbeddingScoreList = (List<Double>) (Object) list;
	}

	public Double getUppercaseScore() {
		return uppercaseScore;
	}

	public void setUppercaseScore(Double uppercaseScore) {
		this.uppercaseScore = uppercaseScore;
	}

	public Double getLetterLengthScore() {
		return letterLengthScore;
	}

	public void setLetterLengthScore(Double letterLengthScore) {
		this.letterLengthScore = letterLengthScore;
	}

	public Double getWordLengthScore() {
		return wordLengthScore;
	}

	public void setWordLengthScore(Double wordLengthScore) {
		this.wordLengthScore = wordLengthScore;
	}

	public Double getTimestampScore() {
		return timestampScore;
	}

	public void setTimestampScore(Double timeScore) {
		this.timestampScore = timeScore;
	}
}
