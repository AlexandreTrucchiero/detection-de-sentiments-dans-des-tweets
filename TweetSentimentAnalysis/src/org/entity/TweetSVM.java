package org.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TweetSVM {
	private String id;
	private List<WordSVM> words;
	private int label;
	public static List<String> lexicon = new LinkedList<>();

	public TweetSVM(String id, String label) {
		super();
		this.id = id;
		this.words = new ArrayList<>();
		switch (label) {
		case "neutral":
			this.label = 0;
			break;
		case "positive":
			this.label = 1;
			break;
		case "negative":
			this.label = 2;
			break;
		case "mixed":
			this.label = 3;
			break;
		default:
			this.label = 0;
			break;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<WordSVM> getWords() {
		return words;
	}

	public void setWords(List<WordSVM> words) {
		this.words = words;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	public void generateOccurrences() {
		Iterator<WordSVM> iter = words.iterator();
		WordSVM lastWord = iter.next();
		WordSVM currWord = null;
		while (iter.hasNext()) {
			currWord = iter.next();
			if (lastWord.getContent().contentEquals(currWord.getContent())) {
				iter.remove();
				lastWord.addOneOccurrence();
			} else {
				lastWord = currWord;
			}
		}
	};

	public void sort() {
		Collections.sort(words);
	}

}
