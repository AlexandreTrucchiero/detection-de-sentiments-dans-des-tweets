package org.entity;

public class WordSVM implements Comparable<Object> {
	private int id;
	private String content;
	private int occurrences;

	public WordSVM(int id, String content) {
		super();
		this.id = id;
		this.content = content;
		this.occurrences = 1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}

	public void addOneOccurrence() {
		this.occurrences++;
	}

	@Override
	public int compareTo(Object o) {
		if (o instanceof WordSVM)
			return this.getId() - ((WordSVM) o).getId();
		else
			return 0;
	}
}
