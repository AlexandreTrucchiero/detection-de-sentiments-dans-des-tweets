package org.performance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.entity.Tweet;
import org.tools.Configuration;
import org.tools.FileManager;
import org.tools.ScoreManager;
import org.tools.TweetManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author alexandre trucchiero
 *
 */
public class PerformanceManager {

	/**
	 * Start all tests
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void start() throws IOException, InterruptedException {

		testDatasets();

		testSVM("training-tweets-2872.json");
		testNN("training-tweets-2872.json");

		testSVM("training-tweets-12462.json");
		testNN("training-tweets-12462.json");
	}

	/**
	 * Test each dataset using TF-IDF representation for tweets and SVM prediction.
	 * Generate results in performance folder
	 * 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void testDatasets() throws JsonParseException, JsonMappingException, IOException {
		Configuration.setTrainingFile("training-tweets-300.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
		Configuration.setTrainingFile("training-tweets-1269.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
		Configuration.setTrainingFile("training-tweets-2872.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
		Configuration.setTrainingFile("training-tweets-4034.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
		Configuration.setTrainingFile("training-tweets-5303.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
		Configuration.setTrainingFile("training-tweets-8628.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
		Configuration.setTrainingFile("training-tweets-12462.json");
		testPerformanceSVM(Arrays.asList("tfIdfScore"));
	}

	/**
	 * Test SVM prediction using several representations for tweets. Generate
	 * results in performance folder
	 * 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void testSVM(String dataset) throws JsonParseException, JsonMappingException, IOException {
		List<String> tfIdf = Arrays.asList("tfIdfScore");
		List<String> allFeatures = Arrays.asList("hashtagScore", "wordScore", "emojiScore", "upperCaseScore",
				"letterLengthScore", "wordLengthScore", "timestampScore");
		List<String> wordEmbedding = Arrays.asList("wordEmbeddingScore");
		List<String> hashtagMeasure = Arrays.asList("hashtagScore");
		List<String> emojiMeasure = Arrays.asList("emojiScore");
		List<String> wordMeasure = Arrays.asList("wordScore");
		List<String> combinedList;

		Configuration.setTrainingFile(dataset);
		testPerformanceSVM(tfIdf);
		testPerformanceSVM(allFeatures);
		testPerformanceSVM(wordEmbedding);

		combinedList = Stream.of(tfIdf, allFeatures).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(tfIdf, wordEmbedding).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(allFeatures, wordEmbedding).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(tfIdf, allFeatures, wordEmbedding).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);

		combinedList = Stream.of(wordEmbedding, hashtagMeasure).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(wordEmbedding, emojiMeasure).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(wordEmbedding, wordMeasure).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);

		combinedList = Stream.of(wordEmbedding, hashtagMeasure, emojiMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(wordEmbedding, hashtagMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(wordEmbedding, emojiMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);

		combinedList = Stream.of(tfIdf, wordEmbedding, hashtagMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(tfIdf, wordEmbedding, emojiMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(tfIdf, wordEmbedding, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);

		combinedList = Stream.of(tfIdf, wordEmbedding, hashtagMeasure, emojiMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(tfIdf, wordEmbedding, hashtagMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);
		combinedList = Stream.of(tfIdf, wordEmbedding, emojiMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceSVM(combinedList);

		combinedList = Stream.of(tfIdf, wordEmbedding, hashtagMeasure, emojiMeasure, wordMeasure)
				.flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceSVM(combinedList);
	}

	/**
	 * Test performance of SVM prediction using scores as input of SVM.
	 * 
	 * @param scores
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void testPerformanceSVM(List<String> scores)
			throws JsonParseException, JsonMappingException, IOException {
		List<Tweet> trainingTweets = FileManager.loadTweetJsonFile(Configuration.TRAINING_FILE_PATH);
		ScoreManager.MODE = "training";
		ScoreManager.computeScores(trainingTweets, scores);
		FileManager.writeTweetFileSVM("svm/train_tweets.svm", trainingTweets, scores);

		List<Tweet> testTweets = FileManager.loadTweetJsonFile(Configuration.TEST_FILE_PATH);
		ScoreManager.MODE = "test";
		ScoreManager.computeScores(testTweets, scores);
		FileManager.writeTweetFileSVM("svm/test_tweets.svm", testTweets, scores);

		// Launch SVM
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command("./svm_launcher.sh");
		try {
			Process process = processBuilder.start();
			int exitVal;
			exitVal = process.waitFor();
			if (exitVal != 0) {
				System.err.println("Error during SVM process!");
				return;
			}
		} catch (IOException | InterruptedException e) {
			System.err.println("Error in SVM process : " + e.getMessage());
		}

		String resultFileName = "performance/"
				+ Configuration.TRAINING_FILE.substring(0, Configuration.TRAINING_FILE.length() - 5) + "/svm-results";
		for (String score : scores)
			resultFileName += "_" + score;
		resultFileName += ".txt";

		FileManager.ResultSVMToResultFile("svm/output", resultFileName, Configuration.TEST_FILE_PATH);
	}

	/**
	 * Test NN prediction using several representations for tweets. Generate results
	 * in performance folder
	 * 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void testNN(String dataset) throws JsonParseException, JsonMappingException, IOException, InterruptedException {
		// List<String> tfIdf = Arrays.asList("tfIdfScore");
		List<String> allFeatures = Arrays.asList("hashtagScore", "wordScore", "emojiScore", "upperCaseScore",
				"letterLengthScore", "wordLengthScore", "timestampScore");
		List<String> wordEmbedding = Arrays.asList("wordEmbeddingScore");
		List<String> hashtagMeasure = Arrays.asList("hashtagScore");
		List<String> emojiMeasure = Arrays.asList("emojiScore");
		List<String> wordMeasure = Arrays.asList("wordScore");
		List<String> combinedList;

		Configuration.setTrainingFile(dataset);
		testPerformanceNN(allFeatures);
		testPerformanceNN(wordEmbedding);

		combinedList = Stream.of(allFeatures, wordEmbedding).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceNN(combinedList);

		combinedList = Stream.of(wordEmbedding, hashtagMeasure).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceNN(combinedList);
		combinedList = Stream.of(wordEmbedding, emojiMeasure).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceNN(combinedList);
		combinedList = Stream.of(wordEmbedding, wordMeasure).flatMap(x -> x.stream()).collect(Collectors.toList());
		testPerformanceNN(combinedList);

		combinedList = Stream.of(wordEmbedding, hashtagMeasure, emojiMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceNN(combinedList);
		combinedList = Stream.of(wordEmbedding, hashtagMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceNN(combinedList);
		combinedList = Stream.of(wordEmbedding, emojiMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceNN(combinedList);

		combinedList = Stream.of(wordEmbedding, hashtagMeasure, emojiMeasure, wordMeasure).flatMap(x -> x.stream())
				.collect(Collectors.toList());
		testPerformanceNN(combinedList);
	}

	/**
	 * Test performance of NN prediction using scores as input of NN.
	 * 
	 * @param scores
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void testPerformanceNN(List<String> scores) throws IOException, InterruptedException {
		// Write training file
		List<Tweet> trainingTweets = FileManager.loadTweetJsonFile(Configuration.TRAINING_FILE_PATH);
		ScoreManager.MODE = "training";
		ScoreManager.computeScores(trainingTweets, scores);
		FileManager.writeTweetFileNN("../NeuralNetwork/train_tweets_nn.txt", trainingTweets, scores);

		// Train NN using training file
		System.out.println("Training Neural Network...");
		ProcessBuilder processBuilder = new ProcessBuilder("python3", "trainStandard.py");
		processBuilder.directory(new java.io.File("../NeuralNetwork"));
		Process process = processBuilder.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = null;
		while ((line = reader.readLine()) != null)
			System.out.println(line);
		int exitVal = process.waitFor();
		if (exitVal != 0) {
			System.err.println("Error during NN training process!");
			return;
		}
		process.destroy();

		List<Tweet> testTweets = FileManager.loadTweetJsonFile(Configuration.TEST_FILE_PATH);
		ScoreManager.MODE = "test";
		ScoreManager.computeScores(testTweets, scores);
		Map<String, String> results = TweetManager.postFindPolarity(testTweets, scores);
		String resultFileName = "performance/"
				+ Configuration.TRAINING_FILE.substring(0, Configuration.TRAINING_FILE.length() - 5) + "/nn-results";
		for (String score : scores)
			resultFileName += "_" + score;
		resultFileName += ".txt";
		FileManager.writeResultFile(resultFileName, results);
	}
}
