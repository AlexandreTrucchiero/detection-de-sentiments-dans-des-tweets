/**
 * 
 */
package org.tools;

/**
 * @author alexandre trucchiero
 *
 */
public abstract class Configuration {

	public static final String INPUT_DIR = "./input/";
	public static final String API_URL = "http://127.0.0.1:3000/";

	public static String TRAINING_FILE = "training-tweets-12462.json";
	public static String TRAINING_FILE_PATH = INPUT_DIR + TRAINING_FILE;
	public static final String TEST_FILE = "test-tweets.json";
	public static final String TEST_FILE_PATH = INPUT_DIR + TEST_FILE;
	
	public static void setTrainingFile(String fileName) {
		Configuration.TRAINING_FILE = fileName;
		Configuration.TRAINING_FILE_PATH = INPUT_DIR + TRAINING_FILE;
	}
}
