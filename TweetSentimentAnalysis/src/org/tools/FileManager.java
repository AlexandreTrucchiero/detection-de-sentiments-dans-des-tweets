package org.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.entity.PolarityObject;
import org.entity.Tweet;
import org.entity.TweetSVM;
import org.entity.WordSVM;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author alexandre trucchiero
 *
 */
public class FileManager {

	/**
	 * Load tweets from an XML file
	 * 
	 * @param path
	 * @return list of tweets
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SAXException
	 * @throws Exception
	 */
	public static List<Tweet> loadTweetXMLFile(String path)
			throws JsonParseException, JsonMappingException, IOException, SAXException, Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();
		System.out.println("Reading tweet file : " + path);
		Document document = builder.parse(new File(path));
		final Element root = document.getDocumentElement();
		final NodeList rootNodes = root.getChildNodes();
		List<Tweet> tweets = new LinkedList<Tweet>();
		int idIncrement = 0;
		for (int i = 0; i < rootNodes.getLength(); i++) {
			if (rootNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				final Element tweetElement = (Element) rootNodes.item(i);
				Tweet tweet = new Tweet(String.valueOf(idIncrement),
						tweetElement.getElementsByTagName("username").item(0).getTextContent(),
						tweetElement.getElementsByTagName("message").item(0).getTextContent(),
						tweetElement.getElementsByTagName("date").item(0).getTextContent());
				tweet.setPolarity(tweetElement.getElementsByTagName("polarity").item(0).getTextContent());
				tweets.add(tweet);
				idIncrement++;
			}
		}
		return tweets;
	}

	/**
	 * Load tweets from a JSON file
	 * 
	 * @param path
	 * @return list of tweets
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static List<Tweet> loadTweetJsonFile(String path)
			throws JsonParseException, JsonMappingException, IOException {
		List<Tweet> tweets = new LinkedList<>();
		File file = new File(path);
		InputStream is = new FileInputStream(file);
		if (file.exists()) {
			JSONTokener tokener = new JSONTokener(is);
			JSONObject object = new JSONObject(tokener);
			JSONArray tweetArray = object.getJSONArray("tweets");
			System.out.println(tweetArray.length());
			int idIncrement = 0;
			String tweetId = null;
			for (int i = 0; i < tweetArray.length(); i++) {
				JSONObject tweetObject = tweetArray.getJSONObject(i);
				if (tweetObject.has("id"))
					tweetId = tweetObject.getString("id");
				else {
					tweetId = String.valueOf(idIncrement);
					idIncrement++;
				}
				Tweet tweet = new Tweet(tweetId, tweetObject.getString("username"), tweetObject.getString("message"),
						tweetObject.getString("date"));
				if (tweetObject.has("polarity"))
					tweet.setPolarity(tweetObject.getString("polarity"));
				tweets.add(tweet);
			}
		}
		return tweets;
	}

	/**
	 * Write tweets in a JSON file
	 * 
	 * @param path
	 * @param tweets
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void writeTweetJsonFile(String path, List<Tweet> tweets)
			throws JsonParseException, JsonMappingException, IOException {
		FileOutputStream fos = new FileOutputStream(new File(path));
		OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
		PrintWriter writer = new PrintWriter(osw);
		JSONObject object = new JSONObject();
		JSONArray tweetArray = new JSONArray();
		for (Tweet tweet : tweets) {
			JSONObject tweetObject = new JSONObject();
			tweetObject.put("id", tweet.getId());
			tweetObject.put("username", tweet.getUser());
			tweetObject.put("message", tweet.getMessage());
			tweetObject.put("date", tweet.getDate());
			tweetObject.put("polarity", tweet.getPolarity());
			tweetArray.put(tweetObject);
		}
		object.put("tweets", tweetArray);
		writer.print(object);
		writer.close();
	}

	/**
	 * Write tweets values to a file with scores to write specified. Format is
	 * adapted to NN
	 * 
	 * @param fileName
	 * @param tweets
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public static void writeTweetFileNN(String fileName, List<Tweet> tweets, List<String> scores)
			throws NumberFormatException, IOException {
		Set<String> scoresSet = new HashSet<>();
		for (String score : scores)
			scoresSet.add(score);
		/*
		 * List<TweetSVM> svmTweets = null; if (scoresSet.contains("tfIdfScore"))
		 * svmTweets = TweetManager.toSVM(tweets);
		 */
		System.out.println("Writing tweets file : " + fileName + "...");
		File file = new File(fileName);
		FileOutputStream fos = new FileOutputStream(file);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
		PrintWriter writer = new PrintWriter(osw);
		for (int k = 0; k < tweets.size(); k++) {
			writer.print(tweets.get(k).getId() + "\t");
			writer.print(tweets.get(k).getPolarity());
			if (scoresSet.contains("wordEmbeddingScore")) {
				for (Double score : tweets.get(k).getWordEmbeddingScoreList())
					writer.print("\t" + score.toString());
			}
			if (scoresSet.contains("hashtagScore")) {
				for (Double score : tweets.get(k).getHashScore())
					writer.print("\t" + score.toString());
			}
			if (scoresSet.contains("emojiScore")) {
				for (Double score : tweets.get(k).getEmojiScore())
					writer.print("\t" + score.toString());
			}
			if (scoresSet.contains("wordScore")) {
				for (Double score : tweets.get(k).getWordScore())
					writer.print("\t" + score.toString());
			}
			if (scoresSet.contains("timestampScore"))
				writer.print("\t" + tweets.get(k).getTimestampScore());
			if (scoresSet.contains("upperCaseScore"))
				writer.print("\t" + tweets.get(k).getUppercaseScore());
			if (scoresSet.contains("letterLengthScore"))
				writer.print("\t" + tweets.get(k).getLetterLengthScore());
			if (scoresSet.contains("wordLengthScore"))
				writer.print("\t" + tweets.get(k).getWordLengthScore());
			/*
			 * if (scoresSet.contains("tfIdfScore")) { int i = 1; for (WordSVM word :
			 * svmTweets.get(k).getWords()) { while(i < word.getId()) { writer.print("\t0");
			 * i++; } writer.print("\t" + word.getOccurrences()); i++; } i++; while(i <=
			 * TweetSVM.lexicon.size()) { writer.print("\t0"); i++; } }
			 */
			writer.print("\t\n");
		}
		writer.close();
		System.out.println("Tweets file written successfully!");
	}

	/**
	 * Write tweets values to a file with scores to write specified. Format is
	 * adapted to SVM.
	 * 
	 * @param fileName
	 * @param tweets
	 * @param scores
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static void writeTweetFileSVM(String fileName, List<Tweet> tweets, List<String> scores)
			throws NumberFormatException, IOException {
		Set<String> scoresSet = new HashSet<>();
		for (String score : scores)
			scoresSet.add(score);
		List<TweetSVM> svmTweets = null;
		if (scoresSet.contains("tfIdfScore"))
			svmTweets = TweetManager.toSVM(tweets);
		System.out.println("Writing tweets file : " + fileName + "...");
		File file = new File(fileName);
		FileOutputStream fos = new FileOutputStream(file);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
		PrintWriter writer = new PrintWriter(osw);
		for (int k = 0; k < tweets.size(); k++) {
			String polarity;
			switch (tweets.get(k).getPolarity()) {
			case "neutral":
				polarity = "0";
				break;
			case "positive":
				polarity = "1";
				break;
			case "negative":
				polarity = "2";
				break;
			case "mixed":
				polarity = "3";
				break;
			default:
				polarity = "0";
				break;
			}
			writer.print(polarity);
			int i = 1;
			if (scoresSet.contains("wordEmbeddingScore")) {
				for (Double score : tweets.get(k).getWordEmbeddingScoreList()) {
					writer.print(" " + i + ":" + score.toString());
					i++;
				}
			}
			if (scoresSet.contains("hashScore")) {
				for (Double score : tweets.get(k).getHashScore()) {
					writer.print(" " + i + ":" + score.toString());
					i++;
				}
			}
			if (scoresSet.contains("emojiScore")) {
				for (Double score : tweets.get(k).getEmojiScore()) {
					writer.print(" " + i + ":" + score.toString());
					i++;
				}
			}
			if (scoresSet.contains("wordScore")) {
				for (Double score : tweets.get(k).getWordScore()) {
					writer.print(" " + i + ":" + score.toString());
					i++;
				}
			}

			if (scoresSet.contains("timestampScore")) {
				writer.print(" " + i + ":" + tweets.get(k).getTimestampScore());
				i++;
			}
			if (scoresSet.contains("upperCaseScore")) {
				writer.print(" " + i + ":" + tweets.get(k).getUppercaseScore());
				i++;
			}
			if (scoresSet.contains("letterLengthScore")) {
				writer.print(" " + i + ":" + tweets.get(k).getLetterLengthScore());
				i++;
			}
			if (scoresSet.contains("wordLengthScore")) {
				writer.print(" " + i + ":" + tweets.get(k).getWordLengthScore());
				i++;
			}
			if (scoresSet.contains("wordLengthScore")) {
				writer.print(" " + i + ":" + tweets.get(k).getWordLengthScore());
				i++;
			}
			if (scoresSet.contains("tfIdfScore")) {
				for (WordSVM word : svmTweets.get(k).getWords())
					writer.print(" " + (word.getId() + i) + ":" + word.getOccurrences());
			}
			writer.print("\n");
		}
		writer.close();
		System.out.println("Tweets file written successfully!");

	}

	/**
	 * Load polarity objects from a file
	 * 
	 * @param path
	 * @return list of polarity objects
	 * @throws IOException
	 */
	public static List<PolarityObject> loadPolarityFile(String path) throws IOException {
		List<PolarityObject> elements = new LinkedList<PolarityObject>();
		BufferedReader br = new BufferedReader(new FileReader(path));
		String str;
		if ((str = br.readLine()) != null)
			while ((str = br.readLine()) != null) {
				String[] cells = str.split("\t");
				elements.add(new PolarityObject(cells[0], Double.parseDouble(cells[1]), Double.parseDouble(cells[2]),
						Double.parseDouble(cells[3]), Double.parseDouble(cells[4])));
			}
		br.close();
		return elements;
	}

	/**
	 * Write a result file formatted to be tested on test web site.
	 * 
	 * @param fileName
	 * @param results
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void writeResultFile(String fileName, Map<String, String> results)
			throws FileNotFoundException, UnsupportedEncodingException {
		System.out.println("Writing result file : " + fileName + "...");
		File file = new File(fileName);
		FileOutputStream fos = new FileOutputStream(file);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
		PrintWriter writer = new PrintWriter(osw);
		Map<String, String> sortedResults = new TreeMap<>(results);
		for (Entry<String, String> entry : sortedResults.entrySet())
			writer.println(entry.getKey() + " " + entry.getValue());

		writer.close();
		System.out.println("Result file written successfully!");
	}

	/**
	 * Convert a SVM result file to a file using a format to be used on test web
	 * site.
	 * 
	 * @param svmFileName
	 * @param resultFileName
	 * @param tweetFileName
	 * @throws IOException
	 */
	public static void ResultSVMToResultFile(String svmFileName, String resultFileName, String tweetFileName)
			throws IOException {

		System.out.println("Loading SVM result file : " + svmFileName + "...");
		BufferedReader reader = new BufferedReader(new FileReader(svmFileName));
		String row = null;
		FileOutputStream fos = new FileOutputStream(new File(resultFileName));
		OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
		PrintWriter writer = new PrintWriter(osw);
		List<Tweet> tweets = FileManager.loadTweetJsonFile(tweetFileName);
		Iterator<Tweet> iter = tweets.iterator();
		System.out.println("Writing result file : " + resultFileName + "...");
		while ((row = reader.readLine()) != null) {
			String result = "";
			if (!iter.hasNext())
				break;
			Tweet tweet = iter.next();
			switch (row) {
			case "0":
				result = "autre";
				tweet.setPolarity("neutral");
				break;
			case "1":
				result = "positif";
				tweet.setPolarity("positive");
				break;
			case "2":
				result = "negatif";
				tweet.setPolarity("negative");
				break;
			case "3":
				result = "mixte";
				tweet.setPolarity("mixed");
				break;
			default:
				result = "autre";
				tweet.setPolarity("neutral");
				break;
			}
			writer.println(tweet.getId() + " " + result);
		}
		reader.close();
		writer.close();
		System.out.println("Result file written successfully!");
	}

	/**
	 * Convert tweet stored in CSV file and write them in a JSON file.
	 * 
	 * @param csvFileName
	 * @param jsonFileName
	 * @throws IOException
	 */
	public static void TweetCSVToJSON(String csvFileName, String jsonFileName) throws IOException {

		System.out.println("Loading CSV tweet file : " + csvFileName + "...");
		BufferedReader reader = new BufferedReader(new FileReader(csvFileName));
		String row = null;
		List<Tweet> tweets = new LinkedList<>();
		String polarity = "";
		while ((row = reader.readLine()) != null) {
			String[] data = row.split("\t");
			switch (data[2]) {
			case "objective":
				polarity = "neutral";
				break;
			case "positive":
				polarity = "positive";
				break;
			case "negative":
				polarity = "negative";
				break;
			case "mixed":
				polarity = "mixed";
				break;
			default:
				polarity = "neutral";
				break;
			}
			Tweet tweet = new Tweet(data[0], "unknown", data[1], "unknown");
			tweet.setPolarity(polarity);
			// System.out.println(tweet.getPolarity());
			tweets.add(tweet);
		}
		reader.close();
		FileManager.writeTweetJsonFile(jsonFileName, tweets);
	}
}
