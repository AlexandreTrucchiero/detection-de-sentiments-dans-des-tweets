package org.tools;

import java.text.Normalizer.Form;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.entity.Tweet;

/**
 * @author alexandre trucchiero
 *
 */
public abstract class Normalizer {

	/**
	 * Normalize a tweet by setting a normalized tokens list
	 * 
	 * @param tweet
	 */
	public static void normalizeTweet(Tweet tweet) {
		List<String> tokens = new LinkedList<>();
		Pattern pattern = Pattern.compile("[\\pL\\pN^#^@]+");
		java.util.regex.Matcher matcher = pattern.matcher(tweet.getMessage());
		String word = null;
		while (matcher.find()) {
			word = normalizeWord(matcher.group());
			tokens.add(word);
		}
		tweet.setTokens(tokens);
	}

	/**
	 * Normalize a string
	 * 
	 * @param str
	 * @return string normalized
	 */
	private static String normalizeWord(String str) {
		String result = null;
		result = java.text.Normalizer.normalize(str, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		result = result.toLowerCase();
		if (result.contentEquals(""))
			result = null;
		return result;
	}

	/**
	 * Apply a filter on a string by using given pattern
	 * 
	 * @param string
	 * @param pattern
	 * @return string filtered
	 */
	public static String filterString(String string, Pattern pattern) {
		String str = "";
		String[] strings = pattern.split(string);
		for (int i = 0; i < strings.length; i++)
			str += strings[i];
		return str;
	}

}
