/**
 * 
 */
package org.tools;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * @author alexandre trucchiero
 *
 */
final class RequestManager {

	/**
	 * Perform post event on given URL providing given JSON string entity
	 * 
	 * @param url
	 * @param jsonStr
	 * @return response
	 */
	static String postRequest(String url, String jsonStr) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		HttpEntity stringEntity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);
		httpPost.setEntity(stringEntity);
		try (CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
			HttpEntity httpEntity = httpResponse.getEntity();
			String response = EntityUtils.toString(httpEntity);
			EntityUtils.consume(httpEntity);
			return response;
		} catch (IOException e) {
			System.err.println("Error during request : " + e.getMessage());
		}
		return "none";
	}
}
