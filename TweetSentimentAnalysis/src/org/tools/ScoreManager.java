package org.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.entity.PolarityObject;
import org.entity.Tweet;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * @author alexandre trucchiero
 *
 */
public abstract class ScoreManager {

	public static String MODE = "training";

	/**
	 * Compute all score measures available sequentially on list of tweets
	 * 
	 * @param tweets
	 * @throws IOException
	 */
	public static void computeAllScores(List<Tweet> tweets) throws IOException {

		ScoreManager.computeHashScore(tweets, Configuration.INPUT_DIR + "hashtag.csv");
		ScoreManager.computeWordScore(tweets, Configuration.INPUT_DIR + "word.csv");
		ScoreManager.computeEmojiScore(tweets, Configuration.INPUT_DIR + "emoji.csv");
		ScoreManager.computeUppercaseScore(tweets);
		ScoreManager.computeLetterLengthScore(tweets);
		ScoreManager.computeWordLengthScore(tweets);
		ScoreManager.computeTimestampScore(tweets);
		ScoreManager.computeWordEmbedding(tweets);
	}

	/**
	 * Compute specified score measures available sequentially on list of tweets
	 * @param tweets
	 * @param scores
	 * @throws IOException
	 */
	public static void computeScores(List<Tweet> tweets, List<String> scores) throws IOException {
		for (String score : scores) {
			switch (score) {
			case "hashScore":
				ScoreManager.computeHashScore(tweets, Configuration.INPUT_DIR + "hashtag.csv");
				break;
			case "wordScore":
				ScoreManager.computeWordScore(tweets, Configuration.INPUT_DIR + "word.csv");
				break;
			case "emojiScore":
				ScoreManager.computeEmojiScore(tweets, Configuration.INPUT_DIR + "emoji.csv");
				break;
			case "upperCaseScore":
				ScoreManager.computeUppercaseScore(tweets);
				break;
			case "letterLengthScore":
				ScoreManager.computeLetterLengthScore(tweets);
				break;
			case "wordLengthScore":
				ScoreManager.computeWordLengthScore(tweets);
				break;
			case "timestampScore":
				ScoreManager.computeTimestampScore(tweets);
				break;
			case "wordEmbeddingScore":
				ScoreManager.computeWordEmbedding(tweets);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Compute a hashtag score based on hashtags polarity for each tweet using a
	 * hashtag-annotated file
	 * 
	 * @param tweets
	 * @param hashtagFile
	 * @throws IOException
	 */
	public static void computeHashScore(List<Tweet> tweets, String hashtagFile) throws IOException {
		List<PolarityObject> hashtags;
		hashtags = FileManager.loadPolarityFile(hashtagFile);
		int nbScore = 0;
		for (Tweet tweet : tweets) {
			nbScore = 0;
			for (PolarityObject hashtag : hashtags) {
				if (tweet.getMessage().contains(hashtag.getName())) {
					tweet.incrementHashScore((double) hashtag.getPositiveScore(), 0);
					tweet.incrementHashScore((double) hashtag.getNegativeScore(), 1);
					tweet.incrementHashScore((double) hashtag.getMixedScore(), 2);
					tweet.incrementHashScore((double) hashtag.getNeutralScore(), 3);
					nbScore++;
				}
			}
			for (int i = 0; i < tweet.getHashScore().length; i++) {
				if (nbScore > 0)
					tweet.setHashScore(tweet.getHashScore(i) / (double) nbScore, i);
				else
					tweet.setHashScore(0d, i);
			}
		}
	}

	/**
	 * Compute a word score based on words polarity for each tweet using a
	 * word-annotated file
	 * 
	 * @param tweets
	 * @param wordFile
	 * @throws IOException
	 */
	public static void computeWordScore(List<Tweet> tweets, String wordFile) throws IOException {
		List<PolarityObject> words;
		words = FileManager.loadPolarityFile(wordFile);
		int nbScore = 0;
		for (Tweet tweet : tweets) {
			nbScore = 0;
			Normalizer.normalizeTweet(tweet);
			for (PolarityObject word : words) {
				for (String token : tweet.getTokens()) {
					if (token.contentEquals(word.getName())) {
						tweet.incrementWordScore((double) word.getPositiveScore(), 0);
						tweet.incrementWordScore((double) word.getNegativeScore(), 1);
						tweet.incrementWordScore((double) word.getMixedScore(), 2);
						tweet.incrementWordScore((double) word.getNeutralScore(), 3);
						nbScore++;
						break;
					}
				}
			}
			for (int i = 0; i < tweet.getWordScore().length; i++) {
				if (nbScore > 0)
					tweet.setWordScore(tweet.getWordScore(i) / (double) nbScore, i);
				else
					tweet.setWordScore(0d, i);
			}
		}
	}

	/**
	 * Compute a emoji score based on emojis polarity for each tweet using a
	 * emoji-annotated file
	 * 
	 * @param tweets
	 * @param emojiFile
	 * @throws IOException
	 */
	public static void computeEmojiScore(List<Tweet> tweets, String emojiFile) throws IOException {
		List<PolarityObject> emojis;
		emojis = FileManager.loadPolarityFile(emojiFile);
		int nbScore = 0;
		for (Tweet tweet : tweets) {
			nbScore = 0;
			Normalizer.normalizeTweet(tweet);
			for (PolarityObject emoji : emojis) {
				for (String token : tweet.getTokens()) {
					if (token.contentEquals(emoji.getName())) {
						tweet.incrementEmojiScore((double) emoji.getPositiveScore(), 0);
						tweet.incrementEmojiScore((double) emoji.getNegativeScore(), 1);
						tweet.incrementEmojiScore((double) emoji.getMixedScore(), 2);
						tweet.incrementEmojiScore((double) emoji.getNeutralScore(), 3);
						nbScore++;
						break;
					}
				}
			}
			for (int i = 0; i < tweet.getEmojiScore().length; i++) {
				if (nbScore > 0)
					tweet.setEmojiScore(tweet.getWordScore(i) / (double) nbScore, i);
				else
					tweet.setEmojiScore(0d, i);
			}
		}
	}

	/**
	 * Compute a uppercase score based on the number of characters in uppercase for
	 * each tweet
	 * 
	 * @param tweets
	 */
	public static void computeUppercaseScore(List<Tweet> tweets) {
		Pattern patternHashtag = Pattern.compile("#[\\pL\\pN]+");
		Pattern patternAt = Pattern.compile("@[\\pL\\pN_]+");
		Pattern patternUrl = Pattern.compile("http\\S+");
		String string = "";
		int countUpperCase = 0;
		for (Tweet tweet : tweets) {
			countUpperCase = 0;
			string = tweet.getMessage();
			string = Normalizer.filterString(string, patternHashtag);
			string = Normalizer.filterString(string, patternAt);
			string = Normalizer.filterString(string, patternUrl);
			for (int i = 0; i < string.length(); i++)
				if (Character.isUpperCase(string.charAt(i)))
					countUpperCase++;
			tweet.setUppercaseScore((double) countUpperCase / (double) string.length());
		}
	}

	/**
	 * Compute a letter length score based on the length in term of characters for
	 * each tweet
	 * 
	 * @param tweets
	 */
	public static void computeLetterLengthScore(List<Tweet> tweets) {
		Pattern patternHashtag = Pattern.compile("#[\\pL\\pN]+");
		Pattern patternAt = Pattern.compile("@[\\pL\\pN_]+");
		Pattern patternUrl = Pattern.compile("http\\S+");
		String string = "";
		for (Tweet tweet : tweets) {
			string = tweet.getMessage();
			string = Normalizer.filterString(string, patternHashtag);
			string = Normalizer.filterString(string, patternAt);
			string = Normalizer.filterString(string, patternUrl);
			tweet.setLetterLengthScore(string.length() / 140d);
			//System.out.println(tweet.getLetterLengthScore());
		}
	}

	/**
	 * Compute a word length score based on the length in term of words for each
	 * tweet
	 * 
	 * @param tweets
	 */
	public static void computeWordLengthScore(List<Tweet> tweets) {
		Pattern patternHashtag = Pattern.compile("#[\\pL\\pN]+");
		Pattern patternAt = Pattern.compile("@[\\pL\\pN_]+");
		Pattern patternUrl = Pattern.compile("http\\S+");
		String string = "";
		String[] words;
		for (Tweet tweet : tweets) {
			string = tweet.getMessage();
			string = Normalizer.filterString(string, patternHashtag);
			string = Normalizer.filterString(string, patternAt);
			string = Normalizer.filterString(string, patternUrl);
			words = string.split(" ");
			tweet.setWordLengthScore(Math.log(words.length));
			// System.out.println(tweet.getWordLengthScore());
		}
	}

	/**
	 * Compute a timestamp score based on tweet post date
	 * 
	 * @param tweets
	 */
	public static void computeTimestampScore(List<Tweet> tweets) {
		for (Tweet tweet : tweets) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date parsedDate;
			try {
				parsedDate = dateFormat.parse(tweet.getDate());
				Timestamp timestamp = new Timestamp(parsedDate.getTime());
				Double score = Math.log((double) (int) (timestamp.getTime() / 60000d) / 1000);
				tweet.setTimestampScore(score);
				//System.out.println(tweet.getTimestampScore());
			} catch (ParseException e) {
				System.err.println("Error : failed to parse tweet date to long timestamp");
			}
		}
	}

	/**
	 * Compute word embedding score for a list of tweets using input file or http
	 * request
	 * 
	 * @param tweets
	 */
	public static void computeWordEmbedding(List<Tweet> tweets) {
		File file;
		if (ScoreManager.MODE.contentEquals("training"))
			file = new File("input/doc2vec-" + Configuration.TRAINING_FILE);
		else
			file = new File("input/doc2vec-" + Configuration.TEST_FILE);
		JSONObject res = new JSONObject();
		try {
			if (file.exists()) {
				InputStream is = new FileInputStream(file);
				System.out.println("Loading word embedding from : " + file.getPath() + "...");
				JSONTokener tokener = new JSONTokener(is);
				res = new JSONObject(tokener);
				is.close();
			} else {
				String action = "message2vec";
				System.out.println("Send tweets to : " + Configuration.API_URL + action + "...");
				JSONObject obj = TweetManager.createJSONObjectMsg(tweets);
				System.out.println("Request word embedding sent...");
				String response = RequestManager.postRequest(Configuration.API_URL + action, obj.toString());
				System.out.println("Word embedding list returned!");
				FileOutputStream fos;
				try {
					if (ScoreManager.MODE.contentEquals("training"))
						fos = new FileOutputStream(new File("input/doc2vec-" + Configuration.TRAINING_FILE));
					else
						fos = new FileOutputStream(new File("input/doc2vec-" + Configuration.TEST_FILE));
					OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
					PrintWriter writer = new PrintWriter(osw);
					writer.print(response);
					writer.close();
				} catch (FileNotFoundException | UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				res = new JSONObject(response);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		List<Object> list;
		for (Tweet tweet : tweets) {
			if (res.has(tweet.getId())) {
				list = res.getJSONArray(tweet.getId()).toList();
				if (list.size() == 20)
					tweet.setWordEmbeddingScoreList(list);
				// System.out.println(tweet.getId() + " : " +
				// tweet.getWordEmbeddingScoreList().toString());
			}
		}
	}
}
