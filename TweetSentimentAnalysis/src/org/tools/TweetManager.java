package org.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.entity.Tweet;
import org.entity.TweetSVM;
import org.entity.WordSVM;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author alexandre trucchiero
 *
 */
public class TweetManager {

	/**
	 * Create JSON object containing tweets values
	 * 
	 * @param tweets
	 * @return object containing tweets values
	 */
	public static JSONObject createJSONObjectScore(List<Tweet> tweets) {
		JSONObject obj = new JSONObject();
		JSONArray array = new JSONArray();
		for (Tweet tweet : tweets) {
			JSONArray tweetArr = new JSONArray();
			tweetArr.put(tweet.getId());
			for (Double score : tweet.getHashScore())
				tweetArr.put(score);
			for (Double score : tweet.getEmojiScore())
				tweetArr.put(score);
			for (Double score : tweet.getWordScore())
				tweetArr.put(score);
			for (Double score : tweet.getSvmScore())
				tweetArr.put(score);
			for (Double score : tweet.getWordEmbeddingScoreList())
				tweetArr.put(score);
			tweetArr.put(tweet.getTimestampScore());
			tweetArr.put(tweet.getUppercaseScore());
			tweetArr.put(tweet.getLetterLengthScore());
			tweetArr.put(tweet.getWordLengthScore());
			array.put(tweetArr);
		}
		obj.put("tweets", array);
		return obj;
	}

	/**
	 * Create JSON object containing tweets values with scores specified
	 * 
	 * @param tweets
	 * @return object containing tweets values
	 */
	public static JSONObject createJSONObjectScore(List<Tweet> tweets, List<String> scores) {
		Set<String> scoresSet = new HashSet<>();
		for (String score : scores)
			scoresSet.add(score);
		System.out.println("SCORE SET : " + scoresSet.toString());
		JSONObject obj = new JSONObject();
		JSONArray array = new JSONArray();
		for (Tweet tweet : tweets) {
			JSONArray tweetArr = new JSONArray();
			tweetArr.put(tweet.getId());
			if (scoresSet.contains("wordEmbeddingScore")) {
				for (Double score : tweet.getWordEmbeddingScoreList())
					tweetArr.put(score);
			}
			if (scoresSet.contains("hashtagScore")) {
				for (Double score : tweet.getHashScore())
					tweetArr.put(score);
			}
			if (scoresSet.contains("emojiScore")) {
				for (Double score : tweet.getEmojiScore())
					tweetArr.put(score);
			}
			if (scoresSet.contains("wordScore")) {
				for (Double score : tweet.getWordScore())
					tweetArr.put(score);
			}
			if (scoresSet.contains("timestampScore"))
				tweetArr.put(tweet.getTimestampScore());
			if (scoresSet.contains("upperCaseScore"))
				tweetArr.put(tweet.getUppercaseScore());
			if (scoresSet.contains("letterLengthScore"))
				tweetArr.put(tweet.getLetterLengthScore());
			if (scoresSet.contains("wordLengthScore"))
				tweetArr.put(tweet.getWordLengthScore());
			array.put(tweetArr);
		}
		obj.put("tweets", array);
		return obj;
	}

	/**
	 * Create JSON object containing tweets values
	 * 
	 * @param tweets
	 * @return object containing tweets values
	 */
	public static JSONObject createJSONObjectMsg(List<Tweet> tweets) {
		JSONObject obj = new JSONObject();
		JSONArray array = new JSONArray();
		for (Tweet tweet : tweets) {
			JSONArray tweetArr = new JSONArray();
			tweetArr.put(tweet.getId());
			tweetArr.put(tweet.getMessage());
			array.put(tweetArr);
		}
		obj.put("tweets", array);
		return obj;
	}

	/**
	 * Find polarity for one tweet
	 * 
	 * @param url
	 * @param tweet
	 */
	public static void postFindPolarity(Tweet tweet) {
		List<Tweet> tweets = new LinkedList<Tweet>();
		tweets.add(tweet);
		TweetManager.postFindPolarity(tweets);
	}

	/**
	 * Find polarity for a list of tweets
	 * 
	 * @param url
	 * @param tweets
	 */
	public static Map<String, String> postFindPolarity(List<Tweet> tweets) {
		String action = "find";
		System.out.println("Send tweets to : " + Configuration.API_URL + action + "...");
		JSONObject obj = TweetManager.createJSONObjectScore(tweets);
		String response = RequestManager.postRequest(Configuration.API_URL + action, obj.toString());

		System.out.println("Request : " + obj.toString());
		System.out.println("Polarity list found : " + response);

		JSONObject res = new JSONObject(response);
		Iterator<String> iter = res.keys();
		Map<String, String> results = new HashMap<>();
		while (iter.hasNext()) {
			String key = iter.next();
			results.put(key, res.getString(key));
		}
		return results;
	}

	/**
	 * Find polarity for a list of tweets with scores specified
	 * 
	 * @param url
	 * @param tweets
	 */
	public static Map<String, String> postFindPolarity(List<Tweet> tweets, List<String> scores) {
		String action = "find";
		System.out.println("Send tweets to : " + Configuration.API_URL + action + "...");
		JSONObject obj = TweetManager.createJSONObjectScore(tweets, scores);
		System.out.println("Request find polarity sent...");
		String response = RequestManager.postRequest(Configuration.API_URL + action, obj.toString());
		System.out.println("Polarity list returned!");
		JSONObject res = new JSONObject(response);
		Iterator<String> iter = res.keys();
		Map<String, String> results = new HashMap<>();
		while (iter.hasNext()) {
			String key = iter.next();
			results.put(key, res.getString(key));
		}
		return results;
	}

	/**
	 * Convert tweets to SVM format
	 * 
	 * @param tweets
	 * @return list of tweets to SVM format
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static List<TweetSVM> toSVM(List<Tweet> tweets) throws NumberFormatException, IOException {
		Pattern pattern = Pattern.compile("[^A-Za-z0-9\"'`@#éàèç]+");

		// Parse file into list of tweets in SVM format
		List<TweetSVM> svmTweets = new LinkedList<>();

		for (Tweet tweet : tweets) {
			String[] words = pattern.split(tweet.getMessage());
			svmTweets.add(new TweetSVM(tweet.getId(), tweet.getPolarity()));
			int index = 0;
			for (String word : words) {
				if (!word.contentEquals("")) {
					if (TweetSVM.lexicon.contains(word)) {
						index = TweetSVM.lexicon.indexOf(word) + 1;
					} else {
						TweetSVM.lexicon.add(word);
						index = TweetSVM.lexicon.size();
					}
					svmTweets.get(svmTweets.size() - 1).getWords().add(new WordSVM(index, word));
				}
			}
			svmTweets.get(svmTweets.size() - 1).sort();
			svmTweets.get(svmTweets.size() - 1).generateOccurrences();

		}
		return svmTweets;
	}
}
